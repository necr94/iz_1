cmake_minimum_required(VERSION 3.13.4)
project(skyscraper)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

enable_testing()

add_subdirectory(project)
