#include <iostream>
#include <cstdlib>

#include "gtest/gtest.h"

extern "C" {
#include "../include/skyscraper.h"
}

using namespace std;

TEST(sort_tests, sort_1) {
  skyscraper* ss_test = (skyscraper*)malloc(3 * sizeof(skyscraper));
  ss_test[0].floors = 13;
  ss_test[0].height = 555;
  ss_test[0].spire_height = 10;
  strcpy(ss_test[0].ss_region, "moscow");
  strcpy(ss_test[0].ss_type, "living");

  ss_test[1].floors = 56;
  ss_test[1].height = 987;
  ss_test[1].spire_height = 18;
  strcpy(ss_test[1].ss_region, "kazan");
  strcpy(ss_test[1].ss_type, "living");

  ss_test[2].floors = 22;
  ss_test[2].height = 436;
  ss_test[2].spire_height = 7;
  strcpy(ss_test[2].ss_region, "moscow");
  strcpy(ss_test[2].ss_type, "living");

  sort_ss(ss_test, 3, 2);
  EXPECT_EQ(strcmp(ss_test[0].ss_region, "kazan"), 0);
  EXPECT_EQ(strcmp(ss_test[1].ss_region, "moscow"), 0);
  EXPECT_EQ(strcmp(ss_test[2].ss_region, "moscow"), 0);
  free(ss_test);
}

TEST(sort_tests, sort_2) {
  skyscraper* ss_test = (skyscraper*)malloc(3 * sizeof(skyscraper));
  ss_test[0].floors = 13;
  ss_test[0].height = 555;
  ss_test[0].spire_height = 10;
  strcpy(ss_test[0].ss_region, "moscow");
  strcpy(ss_test[0].ss_type, "living");

  ss_test[1].floors = 56;
  ss_test[1].height = 987;
  ss_test[1].spire_height = 18;
  strcpy(ss_test[1].ss_region, "kazan");
  strcpy(ss_test[1].ss_type, "buisness");

  ss_test[2].floors = 22;
  ss_test[2].height = 436;
  ss_test[2].spire_height = 7;
  strcpy(ss_test[2].ss_region, "moscow");
  strcpy(ss_test[2].ss_type, "living");

  sort_ss(ss_test, 3, 1);
  EXPECT_EQ(strcmp(ss_test[0].ss_type, "buisness"), 0);
  EXPECT_EQ(strcmp(ss_test[1].ss_type, "living"), 0);
  EXPECT_EQ(strcmp(ss_test[2].ss_type, "living"), 0);
  free(ss_test);
}

int main(int argc, char** argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}