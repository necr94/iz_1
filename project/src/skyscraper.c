#include "../include/skyscraper.h"

int read_ss(skyscraper *ss, const size_t ss_size) {
  if (ss == NULL) {
    return 1;
  }
  for (size_t i = 0; i < ss_size; ++i) {
    if (scanf("%6zu%6zu%6zu%149s%149s",
              &ss[i].floors,
              &ss[i].height,
              &ss[i].spire_height,
              ss[i].ss_type,
              ss[i].ss_region) != 5) {
      return 1;
    }
  }
  return 0;
}

int print_ss(const skyscraper *ss, const size_t ss_size) {
  if (ss == NULL) {
    return 1;
  }
  puts(" Этажи Высота Высота шпиля            Тип        Регион\n");
  for (size_t i = 0; i < ss_size; ++i) {
    if (printf("%6zu%7zu%13zu%14s%14s\n",
               ss[i].floors,
               ss[i].height,
               ss[i].spire_height,
               ss[i].ss_type,
               ss[i].ss_region) < 0) {
      return 1;
    }
  }
  return 0;
}

static int comp_ss_region(const void *a, const void *b) {
  return strcasecmp(((skyscraper *)a)->ss_region, ((skyscraper *)b)->ss_region);
}

static int comp_type(const void *a, const void *b) {
  return strcasecmp(((skyscraper *)a)->ss_type, ((skyscraper *)b)->ss_type);
}

void sort_ss(skyscraper *ss, const size_t ss_size, const int sort_type) {
  if (ss == NULL) {
    return;
  }
  if (sort_type == 1) {
    qsort(ss, ss_size, sizeof(skyscraper), comp_type);
  } else {
    qsort(ss, ss_size, sizeof(skyscraper), comp_ss_region);
  }
}
