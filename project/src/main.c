#include "../include/skyscraper.h"

#define REGION 2
#define TYPE 1

int main(void) {
  puts("init\n");
  size_t array_size;
  if (scanf("%3zu", &array_size) != 1 || array_size == 0) {
    puts("reding error");
    return 1;
  }

  skyscraper *ss_s = malloc(array_size * sizeof(skyscraper));

  if (read_ss(ss_s, array_size) != 0) {
    puts("reading error\n");
    free(ss_s);
    return 1;
  }

  sort_ss(ss_s, array_size, TYPE);

  if (print_ss(ss_s, array_size) != 0) {
    puts("printing error\n");
    free(ss_s);
    return 1;
  }

  puts("---------------------------\n");
  sort_ss(ss_s, array_size, REGION);

  if (print_ss(ss_s, array_size) != 0) {
    puts("printing error\n");
    free(ss_s);
    return 1;
  }
  free(ss_s);
  return 0;
}
