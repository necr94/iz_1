#ifndef PROJECT_INCLUDE_SKYSCRAPER_H_
#define PROJECT_INCLUDE_SKYSCRAPER_H_

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
  size_t floors;
  size_t height;
  size_t spire_height;
  char ss_type[150];
  char ss_region[150];
} skyscraper;

int read_ss(skyscraper *ss, const size_t ss_size);
int print_ss(const skyscraper *ss, const size_t ss_size);
void sort_ss(skyscraper *ss, const size_t ss_size, const int sort_type);

#endif  // PROJECT_INCLUDE_SKYSCRAPER_H_
